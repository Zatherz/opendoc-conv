Open Document Converter
=======================

Open Document Converter is a small shell script using KDialog for GUI that allows for quick conversion of Open Document files to Microsoft Office files using UnoConv, a commandline converter using LibreOffice libraries. Made for use with the Calligra office suite, which sadly does not (yet?) provide an option to export to Office formats from the program.

The program includes easy internalization. Every output is separated into a specific file for translation. Some files can contain special parameters of the format [%%PARAMETER]. 
Translations directory and language can be configured in a special file.

Dependencies 
============ 
* UnoConv 
* Bash 
* KDialog

To install the dependencies on Arch Linux:

    # pacman -S unoconv bash kdebase-kdialog

Usage
=====

    $ opendoc-conv [-d (Debug mode)]

Language and location of translations directory can be configured in opendoc-conv.conf.

TODO
====
* Add Office Open XML types to export options
* Add templates/macros as output/input options
